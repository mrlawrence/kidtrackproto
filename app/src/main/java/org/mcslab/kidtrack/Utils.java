package org.mcslab.kidtrack;

import android.os.Environment;
import android.util.Log;

/**
 * Created by cslin on 2016/9/26.
 */

public final class Utils {

    public static String TAG = "kidtrack" ;

    public static String SVMModelFile = Environment.getExternalStorageDirectory().getAbsolutePath()+"/KidTrack/KidTrackSVMModel.model" ;

    public static int PROPERTY_IBRACON_SCAN_PERIOD_MILLIS = 1000 ;
    public static int PROPERTY_IBEACON_SCAN_WAIT_TIME_MILLIS = 0 ;

    public static void logI(String msg){

        Log.i(TAG, msg);
    }

    public static void logI(int msg){

        Log.i(TAG, ""+msg);
    }

    public static void logI(double msg){

        Log.i(TAG, ""+msg);
    }

    public static void logE(String msg){

        Log.i(TAG, msg);
    }


}
