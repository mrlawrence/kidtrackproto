package org.mcslab.kidtrack;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {


    Intent mSVMService = null;

    TextView mTV = null ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Utils.logI("Hello world");

        mTV = (TextView) findViewById(R.id.TV_position);

        mSVMService = new Intent(this, SVMService.class);
        startService(mSVMService);
        EventBus.getDefault().register(this);


    }



    @Subscribe (threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PositionEvent event) {

        //Utils.logI(event.getMessage());
        mTV.setText(getString(R.string.you_are_on_position,event.getMessage()));


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(mSVMService);
        EventBus.getDefault().unregister(this);

    }
}
