package org.mcslab.kidtrack;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.repackaged.okhttp_v2_2_0.com.squareup.okhttp.internal.Util;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import libsvm.*;


/**
 * Created by cslin on 2016/9/26.
 */

public class SVMService extends Service{

    // for svm
    svm_model SVMModel = null ;
    File SVMModelFile = null ;

    // for iBeacon
    private BeaconManager beaconManager;
    private static final UUID IBEACON_UUID = UUID.fromString("00000000-0000-0000-C000-000000000028");
    private static final Region REGION = new Region("regionId", IBEACON_UUID, null, null);
    //private static final Region REGION = new Region("regionId", null, null, null);

    double time = 0.0 ;
    int index = 0 ;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public SVMService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Utils.logI("SVMService Create");

        // for SVM
        try {
            loadSVMModel();
        } catch (IOException e) {
            e.printStackTrace();
        }

        beaconManager = new BeaconManager(getApplicationContext());

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> list) {

                if(!list.isEmpty()){


                    List<Beacon> beacons = new ArrayList<Beacon>();
                    for(Beacon beacon:list){

                        if(beacon.getMinor()<8000 && beacon.getMinor()>7000){
                            beacons.add(beacon);
                        }
                    }

                    //Utils.logI("beacons size " + beacons.size() );

                    svm_node[] nodes = new svm_node[beacons.size()];


                    for(int i = 0 ; i < beacons.size() ; i++){

                        Beacon beacon = beacons.get(i);
                        //Utils.logI("beacon " + beacon.toString());
                        nodes[i] = new svm_node();
                        nodes[i].index = beacon.getMinor();
                        nodes[i].value = beacon.getRssi();
                        //Utils.logI(nodes[i].index+":"+nodes[i].value);
                    }

                    Arrays.sort(nodes, new Comparator<svm_node>() {
                        @Override
                        public int compare(svm_node o1, svm_node o2) {
                            //return (int)o1.value + (int)o2.value;

                            return o1.index-o2.index ;

                        }
                    });

                    //for(svm_node m : nodes){
                    //    Utils.logI(m.index+":"+m.value);
                    //}



                    long before = System.currentTimeMillis() ;
                    double position = predictPosition(nodes);
                    long after = System.currentTimeMillis() ;
                    time = time + after - before ;
                    index++ ;

                    //Utils.logI(index + " " + time);



                    //Utils.logI("Position : " + position);
                    EventBus.getDefault().post(new PositionEvent(String.valueOf(position)));


                }else{
                    EventBus.getDefault().post(new PositionEvent(String.valueOf(-1)));

                }



            }
        });





        //For the SVM model
/*
        try {
            SVMModel = svm.svm_load_model(Utils.SVMModelFile);

            svm_node[] nodes = new svm_node[10];

            for(int i = 0 ; i < 10 ; i++){
                nodes[i] = new svm_node();
            }


            //5 7544:-75 7545:-96 7546:-96 7547:-84 7558:-84 7560:-72 7561:-90 7562:-84 7566:-87 7571:-91
            //6 7544:-81 7545:-96 7546:-93 7547:-74 7558:-72 7560:-74 7561:-76 7562:-83 7566:-81 7571:-85
            nodes[0].index = 7544 ;
            nodes[0].value = -81;
            nodes[1].index = 7545 ;
            nodes[1].value = -96 ;
            nodes[2].index = 7546 ;
            nodes[2].value = -93 ;
            nodes[3].index = 7547;
            nodes[3].value = -74;
            nodes[4].index = 7558;
            nodes[4].value = -72;
            nodes[5].index = 7560;
            nodes[5].value = -74 ;
            nodes[6].index = 7561;
            nodes[6].value = -76;
            nodes[7].index = 7562;
            nodes[7].value = -83;
            nodes[8].index = 7566;
            nodes[8].value = -81;
            nodes[9].index = 7571;
            nodes[9].value = -85;


            predictPosition(nodes);
            double result = svm.svm_predict(SVMModel, nodes);

            Utils.logI("Result = " + result);

        } catch (IOException e) {
            e.printStackTrace();
        }
    */

    }

    public void loadSVMModel() throws IOException {

        SVMModelFile = new File(Utils.SVMModelFile);
        SVMModel = svm.svm_load_model(Utils.SVMModelFile);

        //int i = svm.svm_check_probability_model(SVMModel);
        //svm_parameter mP = SVMModel.param ;
        //Utils.logI("check" + i + " " + mP.toString());
    }

    public double predictPosition(svm_node[] nodes){

        return svm.svm_predict( SVMModel, nodes);

    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.setForegroundScanPeriod(Utils.PROPERTY_IBRACON_SCAN_PERIOD_MILLIS,Utils.PROPERTY_IBEACON_SCAN_WAIT_TIME_MILLIS);
                beaconManager.startRanging(REGION);
            }
        });

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Utils.logI("SVMService Destroy");

        beaconManager.stopRanging(REGION);
    }
}
