package org.mcslab.kidtrack;

/**
 * Created by cslin on 2016/9/29.
 */

public class PositionEvent {

    private String message ;

    public PositionEvent(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
